package com.example.clevertecapp1

import android.accounts.AuthenticatorDescription
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.clevertecapp1.placeholder.PlaceholderContent

/**
 * A fragment representing a list of Items.
 */
  class ItemFragment : Fragment() {


  override fun onCreateView(
   inflater: LayoutInflater, container: ViewGroup?,
   savedInstanceState: Bundle?
  ): View? {
   val view = inflater.inflate(R.layout.fragment_item_list, container, false)

   view..layoutManager = LinearLayoutManager(activity)
   view.recyclerView.adapter = MyItemRecyclerViewAdapter()


   return view
  }

}