package com.example.clevertecapp1

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.clevertecapp1.databinding.FragmentItemBinding

class MyItemRecyclerViewAdapter : RecyclerView.Adapter<MyItemRecyclerViewAdapter.ItemHolder>()
 {
private val list = ArrayList<ItemFragment>()






     override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_item, parent,false)
        return  ItemHolder(view)
     }

     override fun onBindViewHolder(holder: ItemHolder, position: Int) {
         holder.bind(list[position])

     }

     override fun getItemCount(): Int {
         return list.size
     }
     class ItemHolder(item : View) : RecyclerView.ViewHolder(item) {
         val binding = FragmentItemBinding.bind(item)
         fun bind(item: Item) = with(binding){
             imageView.setImageResource(item.ImageId)
             titleOfItem.text = item.title
             descriptionOfItem.text = item.description

         }

     }
     fun addItem(item : ItemFragment){
         repeat(1000){
             list.add(item)
         }
         notifyDataSetChanged()
     }

 }